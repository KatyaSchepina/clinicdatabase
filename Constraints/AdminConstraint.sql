use MedCenter 
go

alter table dbo.Admin
drop constraint  if exists  Fk_Admin_UserId
go

alter table dbo.Admin 
add constraint Fk_Admin_UserId foreign key(UserId) references dbo.[User](UserId)
go

alter table dbo.Admin
drop constraint  if exists  AK_Admin_UserId_Competence_Description
go

alter table dbo.Admin
add constraint AK_Admin_UserId_Competence_Description unique(UserId,Competence,[Description])
go

