use MedCenter 
go

alter table dbo.Assistent
drop constraint  if exists  Fk_Assistent_UserId
go

alter table dbo.Assistent 
add constraint Fk_Assistent_EmpId foreign key(UserId) references dbo.Employee(EmpId)
go

alter table dbo.Assistent
drop constraint  if exists  Fk_Assistent_SpecialistId
go

alter table dbo.Assistent 
add constraint Fk_Assistent_SpecialistId foreign key(SpecialistId) references dbo.Specialist(SpecialistId)
go

alter table dbo.Assistent
drop constraint  if exists  Fk_Assistent_AdminId
go

alter table dbo.Assistent 
add constraint Fk_Assistent_AdminId foreign key(AdminId) references dbo.Admin(AdminId)
go

alter table dbo.Assistent
drop constraint  if exists AK_Assistent_EmpId_SpecialistId
go

alter table dbo.Assistent 
add constraint AK_Assistent_EmpId_SpecialistId unique(EmpId,SpecialistId)
go