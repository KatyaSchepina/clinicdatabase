use MedCenter 
go


alter table dbo.Conclusion
drop constraint if exists Fk_Conclusion_AssistentId
go


alter table dbo.Conclusion
add constraint Fk_Conclusion_AssistentId foreign key(AssistentId) references dbo.Conclusion(AssistentId)
go


alter table dbo.Conclusion
drop constraint if exists Fk_Conclusion_SpecialistId
go


alter table dbo.Conclusion
add constraint Fk_Conclusion_SpecialistId foreign key(SpecialistId) references dbo.Specialist(SpecialistId)
go



alter table dbo.Conclusion
drop constraint if exists Fk_Conclusion_DiseaseListId
go


alter table dbo.Conclusion
add constraint Fk_Conclusion_DiseaseListId foreign key(DiseaseListId) references dbo.DiseaseList(DiseaseListId)
go


alter table dbo.Conclusion
drop constraint if exists Fk_Conclusion_MedicalCardId
go


alter table dbo.Conclusion
add constraint Fk_Conclusion_MedicalCardId foreign key(MedicalCardId) references dbo.MedicalCard(MedicalCardId)
go


alter table dbo.Conclusion
drop constraint if exists AK_Conclusion_ConclusionDate
go


alter table dbo.Conclusion
add constraint AK_Conclusion_ConclusionDate unique(ConclusionDate)
go