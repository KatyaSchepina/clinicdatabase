use MedCenter 
go


alter table dbo.DiseaseList
drop constraint if exists AK_DiseaseList_Code_Description
go


alter table dbo.DiseaseList
add constraint AK_DiseaseList_Code_Description unique(Code,[Description])
go
