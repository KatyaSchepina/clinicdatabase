use MedCenter 
go

alter table dbo.Employee
drop constraint  if exists  Fk_Employee_UserId
go

alter table dbo.Employee 
add constraint Fk_Employee_UserId foreign key(UserId) references dbo.[User](UserId)
go

alter table dbo.Employee
drop constraint  if exists  Fk_Employee_FilialId
go

alter table dbo.Employee 
add constraint Fk_Employee_FilialId foreign key(FilialId) references dbo.Filial(FilialId)
go

alter table dbo.Employee
drop constraint  if exists  AK_Employee_UserId
go

alter table dbo.Employee
add constraint AK_Employee_UserId unique(UserId)
go
