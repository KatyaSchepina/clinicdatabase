use MedCenter 
go

alter table dbo.Filial
drop constraint  if exists  AK_Filial_City_Street_Home
go

alter table dbo.Filial
add constraint AK_Filial_City_Street_Home unique(City,Street,Home)
go