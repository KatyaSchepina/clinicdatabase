use MedCenter 
go


alter table dbo.Guest
drop constraint  if exists AK_Guest_IpAdress_OperatingSystem_SessionTime_Browser
go


alter table dbo.Guest 
add constraint AK_Guest_IpAdress_OperatingSystem_SessionTime_Browser unique(IpAdress,SessionTime,OperatingSystem,Browser)
go







