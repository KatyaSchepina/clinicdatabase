use MedCenter 
go 

alter table dbo.ManageSchedule
drop constraint if exists Fk_ManageSchedule_ManagerId 
go 


alter table dbo.ManageSchedule
add constraint Fk_ManageSchedule_ManagerId foreign key(ManagerId) references dbo.ManageSchedule(ManagerId)
go


alter table dbo.ManageSchedule
drop constraint if exists Fk_ManageSchedule_ScheduleStaffId
go


alter table dbo.ManageSchedule
add constraint Fk_ManageSchedule_ScheduleStaffId foreign key(ScheduleStaffId) references dbo.ManageSchedule(ScheduleStaffId)
go


alter table dbo.ManageSchedule
drop constraint if exists AK_ManageSchedule_CorrectionDate
go


alter table dbo.ManageSchedule
add constraint AK_ManageSchedule_CorrectionDate unique(CorrectionDate)
go

