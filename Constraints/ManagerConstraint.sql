use MedCenter 
go

alter table dbo.Manager
drop constraint  if exists  Fk_Manager_EmpId
go

alter table dbo.Manager 
add constraint Fk_Manager_EmpId foreign key(EmpId) references dbo.Employee(EmpId)
go

alter table dbo.Manager
drop constraint  if exists  Fk_Manager_AdminId
go

alter table dbo.Manager 
add constraint Fk_Manager_AdminId foreign key(AdminId) references dbo.Admin(AdminId)
go

alter table dbo.Manager
drop constraint  if exists  AK_Manager_EmpId_Competence
go

alter table dbo.Manager
add constraint AK_Manager_EmpId_Competence unique(EmpId,Competence)
go