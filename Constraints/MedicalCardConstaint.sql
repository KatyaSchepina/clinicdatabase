use MedCenter 
go 

alter table dbo.MedicalCard
drop constraint if exists Fk_MedicalCard_UserId
go

alter table dbo.MedicalCard 
add constraint Fk_MedicalCard_UserId foreign key(UserId) references dbo.[User](UserId)
go

alter table dbo.MedicalCard
drop constraint if exists Fk_MedicalCard_AssistentId
go

alter table dbo.MedicalCard
add constraint Fk_MedicalCard_AssistentId foreign key(AssistentId) references dbo.Assistent(AssistentId)
go

alter table dbo.MedicalCard
drop constraint if exists AK_MedicalCard_UserId
go

alter table dbo.MedicalCard
add constraint AK_MedicalCard_UserId unique(UserId)
go




