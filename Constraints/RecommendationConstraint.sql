use MedCenter 
go 


alter table dbo.Recommendation
drop constraint if exists Fk_Recommendation_ConclusionId
go


alter table dbo.Recommendation
add constraint Fk_Recommendation_ConclusionId foreign key(ConclusionId) references dbo.Conclusion(ConclusionId)
go


alter table dbo.Recommendation
drop constraint if exists AK_Recommendation_ConlusionId_Description
go


alter table dbo.Recommendation
add constraint AK_Recommendation_ConlusionId_Description unique(ConclusionId,[Description])
go


