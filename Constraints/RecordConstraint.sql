use MedCenter 
go

alter table dbo.Record
drop constraint  if exists  Fk_Record_UserId
go

alter table dbo.Record 
add constraint Fk_Record_UserId foreign key(UserId) references dbo.[User](UserId)
go

alter table dbo.Record
drop constraint  if exists  Fk_Record_SpecialistId
go

alter table dbo.Record 
add constraint Fk_Record_SpecialistId foreign key(SpecialistId) references dbo.Specialist(SpecialistId)
go

alter table dbo.Record
drop constraint  if exists  Fk_Record_ManagerId
go

alter table dbo.Record 
add constraint Fk_Record_ManagerId foreign key(ManagerId) references dbo.Manager(ManagerId)
go

alter table dbo.Record
drop constraint  if exists  AK_Record_UserId_RecordTime
go

alter table dbo.Record
add constraint AK_Record_UserId_RecordTime unique(UserId,RecordTime)
go