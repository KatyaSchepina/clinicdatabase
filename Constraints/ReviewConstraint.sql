use MedCenter 
go

alter table dbo.Review
drop constraint  if exists  Fk_Review_UserId
go

alter table dbo.Review 
add constraint Fk_Review_UserId foreign key(UserId) references dbo.[User](UserId)
go

alter table dbo.Review
drop constraint  if exists  Fk_Review_RecordId
go

alter table dbo.Review 
add constraint Fk_Review_RecordId foreign key(RecordId) references dbo.Record(RecordId)
go

alter table dbo.Review
drop constraint  if exists  Fk_Review_ManagerId
go

alter table dbo.Review 
add constraint Fk_Review_ManagerId foreign key(ManagerId) references dbo.Manager(ManagerId)
go

alter table dbo.Review
drop constraint  if exists  AK_Review_UserId_RecordId_ManagerId
go

alter table dbo.Review
add constraint AK_Review_UserId_RecordId_ManagerId unique(UserId, RecordId, managerId)
go