use MedCenter 
go

alter table dbo.ScheduleStaff
drop constraint if exists Fk_ScheduleStaff_EmployeeId
go

alter table dbo.ScheduleStaff
add constraint Fk_ScheduleStaff_EmployeeId foreign key(EmployeeId) references dbo.Employee(EmployeeId)
go


alter table dbo.ScheduleStaff
drop constraint if exists AK_ScheduleStaff_EmployeeId_Day 
go

alter table dbo.ScheduleStaff
add constraint AK_ScheduleStaff_EmployeeId_Day unique(EmployeeId,[Day])