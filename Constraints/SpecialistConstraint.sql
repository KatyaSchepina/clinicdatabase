use MedCenter 
go

alter table dbo.Specialist
drop constraint  if exists  Fk_Specialist_EmpId
go

alter table dbo.Specialist 
add constraint Fk_Specialist_EmpId foreign key(EmpId) references dbo.Employee(EmpId)
go

alter table dbo.Specialist
drop constraint  if exists  Fk_Specialist_AdminId
go

alter table dbo.Specialist 
add constraint Fk_Specialist_AdminId foreign key(AdminId) references dbo.Admin(AdminId)
go

alter table dbo.Specialist
drop constraint  if exists  AK_Specialist_EmpId
go

alter table dbo.Specialist
add constraint AK_Specialist_EmpId unique(EmpId)
go