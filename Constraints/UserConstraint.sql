use MedCenter 
go

alter table dbo.[User]
drop constraint  if exists  AK_User_Email
go

alter table dbo.[User]
add constraint AK_User_Email unique(Email)
go

alter table dbo.[User]
drop constraint  if exists  AK_User_PassportNumber
go

alter table dbo.[User]
add constraint AK_User_PassportNumber unique(PassportNumber)
go