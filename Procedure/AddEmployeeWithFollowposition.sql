use MedCenter
go


drop procedure if exists dbo.AddEmployeWithFollowPosition
go

create procedure dbo.AddEmployeWithFollowPosition
       @parametrs nvarchar(max),
       @IdEmployee int
as
begin
    declare @AdminEmail varchar(30)
    select @AdminEmail = json_value (@parametrs,'$.Admin.User.Email')
	declare @EmployeeEmail varchar(30)

	select @EmployeeEmail = json_value (@parametrs,'$.Employee.Specialist.Email')
    declare @AdminId int

    select @AdminId = AdminId 
    from dbo.[Admin],dbo.[User] 
    where dbo.[User].Email = @AdminEmail

    declare @Position varchar(30)
    select @Position = json_value (@parametrs,'$.Employee.Position')
    if(@Position = 'Manager')
	begin
        insert into dbo.Manager(EmployeeId,AdminId)
        values(@IdEmployee,@AdminId)
	end
    if(@Position = 'Specialist')
	begin
        insert into dbo.Specialist(EmployeeId,AdminId)
        values(@IdEmployee,@AdminId)
	end
     if(@Position = 'Assistent')
	 begin
		declare @SpecialistId int
		select @SpecialistId=  S.SpecialistId from dbo.Specialist as S
		inner join dbo.Employee as E on E.EmployeeId = S.EmployeeId
		inner join dbo.[User] as U on E.UserId = U.UserId
        where U.Email = @EmployeeEmail
        insert into dbo.Assistent(EmployeeId,SpecialistId,AdminId)
        values(@IdEmployee,@SpecialistId,@AdminId)
	 end
end
go