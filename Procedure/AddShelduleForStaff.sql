use MedCenter
go


-- exec dbo.AddShelduleStaff @parametrs=N'
--{
--    "Employee": {
--        "User": {
--            "Email": "RosellaBarnett@gmail.com"
--        }
--    },
--    "StaffWorkSchedule": {
--        "Day": "2020-08-28",
--        "StartTime": "11:00:00",
--        "EndTime": "17:00:00"
--    }
--}'
   
drop procedure if exists dbo.AddShelduleStaff
go


create procedure dbo.AddShelduleStaff
	@parametrs nvarchar(max)
as
begin	
	declare @EmployeeId int 
	declare @Employe int 
	exec  dbo.ReturnIdThroughEmail @parametrs, @EmployeeId output

	select @Employe = E.EmployeeId from dbo.Employee as E
	inner join dbo.[User] as U on U.UserId = E.UserId
	where U.UserId = @EmployeeId
	insert into dbo.ScheduleStaff(EmployeeId,[Day],StartTime,EndTime)
	select 
	@Employe,
	js.[Day],
	js.StartTime,
	js.EndTime
	from 
	openjson(@parametrs)
	with
	(
		[Day]     date        '$.StaffWorkSchedule.Day',
		StartTime datetime    '$.StaffWorkSchedule.StartTime',
		EndTime   datetime    '$.StaffWorkSchedule.EndTime'
	)js
	
end
go