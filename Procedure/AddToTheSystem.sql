use MedCenter
go


--  exec dbo.AddToTheSystemEmployees @parametrs=N'
--{
--    "Admin": {
--        "User": {
--            "Email": "RhodesFrancis@gmail.com"
--        },
--        "Competence":"Main Admin",
--        "Description":"Main admin who add all employees in system"
--    },
--    "Employee": {
--        "Desctiption": "AssistentPosition",
--        "Position": "Assistent",
--        "Specialist": {
--            "Email": "StuartPhillips@gmail.com"
--        },
--        "DateStart": "2020-05-08",
--        "Status": true,
--        "Filial": {
--            "City": "Brest",
--            "Street": "Surganov",
--            "Home": 47
--        },
--        "User": {
--            "Email": "KatyaSchepina@gmail.com"
--        }
--    }
--}
--  '
    

drop procedure if exists dbo.AddToTheSystemEmployees
go


create procedure dbo.AddToTheSystemEmployees
	@parametrs nvarchar(max)
as
begin

	declare @foundId int
	exec  dbo.ReturnIdThroughEmail @parametrs,@foundId output
	declare @FilialId int 
	exec dbo.GetIdFilial @parametrs,@FilialId output
	create table #Employee
	(
		EmployeeId int identity(1,1),
		UserId int not null,
		FilialId int not null,
		Position nvarchar(30) not null,
		[Description] nvarchar(1000) null,
		[Status] bit not null,
		DateStart datetime not null,
	)


	insert into #Employee
		(
		UserId ,
		FilialId ,
		Position ,
		[Description],
		[Status],
		DateStart
		)
	select
		@foundId,
		@FilialId,
		js.Position,
		js.[Description],
		js.[Status],
		js.DateStart
	from openjson(@parametrs)
	with
		(
			   [Description]    nvarchar(1000)    '$.Employee.Desctiption',
               Position         nvarchar(30)      '$.Employee.Position',
               [Status]         bit               '$.Employee.Status',
               DateStart        datetime          '$.Employee.DateStart'
		)js


	insert into dbo.Employee
		(
		UserId,
		FilialId,
		Position,
		[Description],
		[Status],
		DateStart
		)
	select
		UserId,
		FilialId ,
		Position ,
		[Description],
		[Status] ,
		DateStart
	from #Employee


	declare @EmployeeId int


	select @EmployeeId = EmployeeId
	from dbo.Employee
	where UserId = @foundId

	exec dbo.AddEmployeWithFollowPosition @parametrs,@EmployeeId
end
go