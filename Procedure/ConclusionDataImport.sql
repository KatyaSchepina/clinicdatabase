use MedCenter 
go



-- exec dbo.ConclusionDataImport @parametrs=N'
-- {
--    "MedicalCard": {
--        "CreatedDate": "2019-08-12 11:12:28",
--        "User": {
--            "Email": "OlegBabichev@gmail.com"
--        }
--    },
--    "Employee": {
--        "User": {
--            "Email": "ClaraGrimes@gmail.com"
--        }
--    },
--    "Conclusion": {
--        "DateTime": "2019-12-12 11:12:28",
--        "Description": "Descriptionasdasdasdasdasdasdas"
--    },
--    "Diseases": {
--        "Code": 4
--    }
--}'

drop procedure if exists dbo.ConclusionDataImport
go


create procedure dbo.ConclusionDataImport
	@parametrs nvarchar(max)
as
begin
	declare @DiseasesId int 
	declare @CodeDiseases int
	declare @CreatedData datetime
	declare @MedCardId int
	declare @AssistentId int 
	declare @SpecialistId int 

	declare @EmployeeId int 

	select @CodeDiseases = json_value (@parametrs,'$.Diseases.Code')

	select @DiseasesId = DiseaseListId from dbo.DiseaseList as D
	where D.Code = @CodeDiseases

	select @CreatedData = json_value (@parametrs,'$.MedicalCard.CreatedDate')

	select @MedCardId = MedicalCardId 
	from dbo.MedicalCard  as MedCard
	where MedCard.CreateDate = @CreatedData

	exec  dbo.ReturnIdThroughEmail @parametrs, @EmployeeId output

	select @AssistentId = A.AssistentId from dbo.Assistent as A 
	inner join dbo.Employee as E on A.EmployeeId  = E.EmployeeId
	inner join dbo.[User] as U on U.UserId =E.UserId
	where U.UserId  = @EmployeeId

	select @SpecialistId = A.SpecialistId from dbo.Assistent as A 
	where A.AssistentId =@AssistentId 

	insert into dbo.Conclusion(MedicalCardId,AssistentId,SpecialistId,DiseaseId,ConclusionDate,[Description])
	select 
	@MedCardId,
	@AssistentId,
	@SpecialistId,
	@DiseasesId,
	js.ConclusionDate,
	js.[Description]
	from openjson(@parametrs)
	with
	(
	    ConclusionDate   datetime            '$.Conclusion.DateTime',
		[Description]    nvarchar(1000)      '$.Conclusion.Description'    
	)js

end
go