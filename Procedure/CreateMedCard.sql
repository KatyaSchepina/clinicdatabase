use MedCenter
go

--exec dbo.CreatingMedCard @parametrs=N'
--{
--    "MedicalCard": {
--        "CreatedDate": "2019-08-12 11:12:28",
--        "User": {
--            "Email": "OlegBabichev@gmail.com"
--        }
--    },
--    "Employee": {
--        "User": {
--            "Email": "ClaraGrimes@gmail.com"
--        }
--    }
--}
--'


drop procedure if exists dbo.CreatingMedCard
go



create procedure dbo.CreatingMedCard
     @parametrs varchar(max)
as
begin 
    declare @foundIdEmployee int
	declare @foundIdClient int 
	declare @AssistentId int 
	exec GetClientdId @parametrs,@foundIdClient output
	exec  dbo.ReturnIdThroughEmail @parametrs,@foundIdEmployee output

	select @AssistentId = A.AssistentId from dbo.Assistent as A
	inner join dbo.Employee as E on E.EmployeeId = A.EmployeeId
	inner join dbo.[User] as U on U.UserId = E.UserId
	where U.UserId = @foundIdEmployee

	
	insert into dbo.MedicalCard(UserId,AssistentId,CreateDate)
	select 
	@foundIdClient,
	@AssistentId,
	js.CreateDate
	from openjson(@parametrs)
	with
	(
		CreateDate    datetime     '$.MedicalCard.CreatedDate'    
	)js

end
go