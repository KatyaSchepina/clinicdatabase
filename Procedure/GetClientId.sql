use MedCenter 
go


drop procedure if exists dbo.GetClientdId
go


create procedure dbo.GetClientdId
	@parametrs nvarchar(max),
	@IdUser int output
as
begin
	create table #Email
	(
		EmailId int identity(1,1),
		Email varchar(30) not null
	)
	insert into #Email
		(
		Email
		)
	select
		jq.Email
	from openjson(@parametrs)
	with
		(
			Email    varchar(30)		'$.MedicalCard.User.Email'
		)jq

	select @IdUser = UserId
	from dbo.[User], #Email as TimeEmail
	where dbo.[User].Email=TimeEmail.Email

end
go