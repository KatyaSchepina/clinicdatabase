use MedCenter
go


drop procedure if exists dbo.GetIdFilial
go


create procedure dbo.GetIdFilial
    @parametrs nvarchar(max),
	@IdFilial int output
as 
begin
    create table #Filial
    (
         FilialId    int              identity(1,1),
         City        nvarchar(20)     not null,
         Street      nvarchar(50)     not null,
         Home        int              not null,
    )
    insert into #Filial
		(
		City,
        Street,
        Home
		)
	select
		jq.City,
        jq.Street,
        jq.Home
	from openjson(@parametrs)
	with
		(
			City    nvarchar(20)     '$.Employee.Filial.City',
            Street  nvarchar(50)     '$.Employee.Filial.Street',
            Home    int              '$.Employee.Filial.Home'
		)jq
    select @IdFilial = 
    F.FilialId from dbo.Filial as F,#Filial as TimeF
    where F.City = TimeF.City and 
    F.Home = TimeF.Home and 
    F.Street = TimeF.Street 
end
go