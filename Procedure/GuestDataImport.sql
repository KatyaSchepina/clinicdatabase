use MedCenter
go


drop procedure if exists dbo.GuestDataImport
go



create procedure dbo.GuestDataImport
	@parametrs nvarchar(max)
as
begin
	set nocount on
	insert into dbo.Guest
	select *
	from openjson(@parametrs,'$.Guest')
	with
	(
		IpAdress            nvarchar(12)    '$.IPv4',
		SessionTime         datetime        '$.SessionConnect',
		Browser             nvarchar(15)    '$.Browser',
		OperatingSystem     nvarchar(17)    '$.OS',
		Flag                nvarchar(14)    '$.Flag'
	)
end
go