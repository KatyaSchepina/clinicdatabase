use MedCenter 
go

--exec dbo.ManagerSheldule @parametrs=N'
--{
--    "Employee": {
--        "User": {
--            "Email":"MurphyCarr@gmail.com"
--        }
--    },
--    "StaffWorkSchedule": {
--        "Day": "2020-08-28",
--        "StartTime": "11:00:00",
--        "EndTime": "17:00:00"
--    }
--}'


drop procedure if exists dbo.ManagerSheldule
go

create procedure dbo.ManagerSheldule
    @parametrs nvarchar(max)
as
begin
    set nocount on

    declare @ScheduleStaff int
    declare @ManagerId int
	declare @EmloyeeEmail nvarchar(30)
	select @EmloyeeEmail = json_value (@parametrs,'$.Employee.User.Email')
    select @ScheduleStaff = S.ScheduleStaffId
    from dbo.ScheduleStaff as S
    where S.[Day] = json_value (@parametrs,'$.StaffWorkSchedule.Day') and
          S.[StartTime] =  json_value (@parametrs,'$.StaffWorkSchedule.StartTime') and 
          S.EndTime = json_value (@parametrs,'$.StaffWorkSchedule.EndTime')

    select @ManagerId = M.ManagerId
    from dbo.Manager as M
        inner join dbo.Employee as E on E.EmployeeId = M.EmployeeId
        inner join dbo.[User] as U on U.UserId = E.UserId
    where U.Email = @EmloyeeEmail


	insert into dbo.ManageSchedule(ManagerId,ScheduleStaffId,CorrectionDate)
    select
	@ManagerId,
	@ScheduleStaff,
	js.CorrectionDate
	from openjson(@parametrs)
	with
	(
		CorrectionDate date    '$.StaffWorkSchedule.Day'   
	)js

end
go