use MedCenter
go


-- exec dbo.RecordDataImport @parametrs=N'
--{
--    "User": {
--        "Email": "OlegBabichev@gmail.com"
--    },
--    "Employee": {
--        "User": {
--            "Email": "StuartPhillips@gmail.com"
--        }
--    },
--    "Appointment": {
--        "Day": "2020-08-31",
--        "Time": "12:00:00"
--    }
--}'


drop procedure if exists dbo.RecordDataImport
go



create procedure  dbo.RecordDataImport
	@parametrs nvarchar(max)
as
begin
    declare @ClientId int
	declare @ClientEmail varchar(30)
	declare @EmployeeEmail varchar(30)
	declare @SpecialistId int

	
	select @EmployeeEmail =  json_value(@parametrs,'$.Employee.User.Email')
	select @SpecialistId=  S.SpecialistId from dbo.Specialist as S
	inner join dbo.Employee as E on E.EmployeeId = S.EmployeeId
	inner join dbo.[User] as U on E.UserId = U.UserId
	where U.Email = @EmployeeEmail


	
	select @ClientEmail = json_value(@parametrs,'$.User.Email')
	select @ClientId = UserId from dbo.[User] as U 
	where U.Email = @ClientEmail

	insert into dbo.Record (UserId,SpecialistId,[Day],[Time]  )
	select
	@ClientId,
	@SpecialistId,
	js.[Day],
	js.[Time]
	from openjson(@parametrs)
	with
	(
	   [Day]      datetime   '$.Appointment.Day',
	   [Time]      time      '$.Appointment.Time'
	)js
	
end
go