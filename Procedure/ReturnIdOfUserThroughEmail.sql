use MedCenter 
go


drop procedure if exists dbo.ReturnIdThroughEmail
go


create procedure dbo.ReturnIdThroughEmail
	@parametrs nvarchar(max),
	@IdUser int output
as
begin
	create table #Email
	(
		EmailId int identity(1,1),
		Email varchar(30) not null
	)
	insert into #Email
		(
		Email
		)
	select
		jq.Email
	from openjson(@parametrs)
	with
		(
			Email    varchar(30)		'$.Employee.User.Email'
		)jq

	select @IdUser = UserId
	from dbo.[User], #Email as TimeEmail
	where dbo.[User].Email=TimeEmail.Email

end
go