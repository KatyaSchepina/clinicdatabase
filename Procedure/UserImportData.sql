use MedCenter
go


--exec dbo.RegistrationDataImport @parametrs=N'
--{
--    "Guest": {
--      "OS": "Wiindows 8",
--      "Browser": "Chrome",
--      "IPv4": "180.185.3.89",
--      "SessionConnect": "2007-05-08 12:35:29",
--      "Flag":"Registration"
--    },
--    "User": {
--      "FirstName": "Katya",
--      "LastName": "Schepina",
--      "Phone": "9614893239",
--      "Passport": "12374A34150PB4",
--      "PasswordHashSHA1": "E346CC09F4772B4151CF1D03E7B406C6EC8FB9EA",
--      "Email": "KatyaSchepina@gmail.com"
--    }
--  }
-- '

drop procedure if exists dbo.RegistrationDataImport
go


create procedure dbo.RegistrationDataImport
  @parametrs nvarchar(max)
as
begin
  set nocount on

  exec dbo.GuestDataImport @parametrs

  insert into dbo.[User]
  select *
  from openjson(@parametrs,'$.User')
	with
	(
	    Email             nvarchar(30)    '$.Email',
        PassportNumber    nvarchar(14)    '$.Passport',
        FirstName         nvarchar(15)    '$.FirstName',
        LastName          nvarchar(20)    '$.LastName',
        Phone             nvarchar(15)    '$.Phone'
	)
end
go
