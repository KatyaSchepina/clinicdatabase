use MedCenter
go

--exec dbo.WriteReview @parametrs=N'
--{
--    "User": {
--        "Email": "OlegBabichev@gmail.com"
--    },
--    "Employee": {
--        "User": {
--            "Email": "MurphyCarr@gmail.com"
--        }
--    },
--    "Appointment": {
--        "Day": "2020-08-26",
--        "Time": "14:00:00"
--    },
--    "Review": {
--        "Aprove": false,
--        "TextReview": "Best Servises that i attend"
--    }
--}
--'

drop procedure if exists dbo.WriteReview
go

create procedure dbo.WriteReview
    @parametrs nvarchar(max)
as
begin
    set nocount on

    declare @EmloyeeEmail varchar(30)
    declare @ClientEmail varchar(30)
    declare @ManagerId int
    declare @RecordId int
    declare @UserId int

    select @EmloyeeEmail = json_value (@parametrs,'$.Employee.User.Email')

    select @ClientEmail = json_value (@parametrs,'$.User.Email')

    select @UserId = U.UserId
    from dbo.[User] as U
    where U.Email = @ClientEmail

    select @ManagerId = M.ManagerId
    from dbo.Manager as M
        inner join dbo.Employee as E on E.EmployeeId = M.EmployeeId
        inner join dbo.[User] as U on U.UserId = E.UserId
    where U.Email = @EmloyeeEmail



    select @RecordId = R.RecordId
    from dbo.Record as R
    where R.[Day] = json_value (@parametrs,'$.Appointment.Day') and R.[Time] =  json_value (@parametrs,'$.Appointment.Time')

   

     insert into dbo.Review(UserId,RecordId,ManagerId,IsApproved,[Description])
     select 
     @UserId,
     @RecordId,
	 @ManagerId,
	 js.IsApproved,
	 js.[Description]
	 from openjson(@parametrs)
	 with 
	 (
		IsApproved       bit               '$.Review.Aprove',
        [Description]    nvarchar(1000)    '$.Review.TextReview'
	 )js

end
go


