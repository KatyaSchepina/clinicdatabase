﻿use Medcenter
go

--посчитать, сколько приемов за прошедший месяц
drop procedure if exists CountRecordsOnMonth;
go

create procedure CountRecordsOnMonth
as
begin

	declare @PivotColumnNames as nvarchar(max),
            @PivotSelectColumnNames as nvarchar(max),
		    @query  as nvarchar(max)


	select @PivotColumnNames= isnull(@PivotColumnNames + ',','')
           + quotename("Full Name")
	from (select concat(FirstName,' ',LastName) as "Full Name", COUNT(*) as "Count of Record", datepart(month,dateadd(month, -1, GETDATE())) as Month, datepart(year,dateadd(month, -1, GETDATE())) as Year
		from dbo.Record  as R
			inner join dbo.Specialist as S on S.SpecialistId = R.SpecialistId
			inner join dbo.Employee as E on E.EmployeeId = S.EmployeeId
			inner join dbo.[User] as U on U.UserId = E.UserId
		where [Day] > dateadd(month, -1, GETDATE())
		group by FirstName,LastName ) as Dates

	set @query =  

				N'select Month,Year, ' + @PivotColumnNames + '
				from
				(
					select 
					concat(FirstName,'' '',LastName) as "Full Name", 
					COUNT(*) as "Count of Record", 
					datepart(month,dateadd(month, -1, GETDATE())) as Month, datepart(year,dateadd(month, -1, GETDATE())) as Year 
					from dbo.Record  as R
					inner join dbo.Specialist as S on S.SpecialistId = R.SpecialistId
					inner join dbo.Employee as E on E.EmployeeId = S.EmployeeId
					inner join dbo.[User] as U on U.UserId = E.UserId
					where [Day] > dateadd(month, -1, GETDATE())
					group by FirstName,LastName
				)s
				pivot 
				(
					max([Count of Record])
					for [Full Name] in (' + @PivotColumnNames + ')
				)p'

	execute(@query)
end
go
EXEC CountRecordsOnMonth;

