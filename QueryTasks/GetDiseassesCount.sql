use MedCenter 
go


drop procedure if exists dbo.GetStatiscitOfDiseases
go

create procedure dbo.GetStatiscitOfDiseases
as
begin
    declare  @DynamicPivotQuery as nvarchar(max),
            @PivotColumnNames as nvarchar(max),
            @PivotSelectColumnNames as nvarchar(max),
		    @query  as nvarchar(max)

    select @PivotColumnNames= isnull(@PivotColumnNames + ',','')
           + quotename([Full Name])
    from (select distinct concat(FirstName,' ',LastName) as "Full Name"
        from dbo.Conclusion  as C
            inner join dbo.DiseaseList as D on D.DiseaseListId = C.DiseaseId
            inner join dbo.MedicalCard as M on C.MedicalCardId = M.MedicalCardId
            inner join dbo.[User] as U on U.UserId = M.UserId) as Dates


    select @PivotSelectColumnNames 
    =      isnull(@PivotSelectColumnNames + ',','')
           + 'isnull(' + quotename([Full Name] ) + ',' + ''''') as '
           + quotename([Full Name] )
    from (select distinct concat(FirstName,' ',LastName) as "Full Name"
        from dbo.Conclusion  as C
            inner join dbo.DiseaseList as D on D.DiseaseListId = C.DiseaseId
            inner join dbo.MedicalCard as M on C.MedicalCardId = M.MedicalCardId
            inner join dbo.[User] as U on U.UserId = M.UserId) as Dates
    

    set @query = 'select Description, ' + @PivotSelectColumnNames + ' from 
            (
           select concat(FirstName,'' '',LastName) as "Full Name",D.Description ,COUNT(D.DiseaseListId)  as "How much diseased" from dbo.Conclusion  as C
			inner join dbo.DiseaseList as D on D.DiseaseListId = C.DiseaseId
			inner join dbo.MedicalCard as M on C.MedicalCardId = M.MedicalCardId
			inner join dbo.[User] as U on U.UserId = M.UserId
			group by D.Description,FirstName,LastName
           ) x
            pivot 
            (
                max([How much diseased])
               	for [Full Name] in (' + @PivotColumnNames + ')
            ) p'


    execute(@query)

end
go