use MedCenter
go

drop procedure dbo.GetGraphic
go

create procedure dbo.GetGraphic
    @ForDays int
as
begin
    declare @DynamicPivotQuery as nvarchar(max),
            @PivotColumnNames as nvarchar(max),
            @PivotSelectColumnNames as nvarchar(max),
		    @query  as nvarchar(max)


    select @PivotColumnNames= isnull(@PivotColumnNames + ',','')
           + quotename([Day])
    from (select distinct [Day]
        from ScheduleStaff
        where (([Day] < dateadd(day,@ForDays,getdate())  ) and [Day] >= getdate()) ) as Dates


    select @PivotSelectColumnNames 
    =      isnull(@PivotSelectColumnNames + ',','')
           + 'isnull(' + quotename([Day] ) + ',' + ''''') as '
           + quotename([Day] )
    from (select distinct [Day]
        from ScheduleStaff
        where (([Day] < dateadd(day,@ForDays,getdate())  ) and [Day] >= getdate()) ) as Dates

    set @query = 'select FirstName,LastName, ' + @PivotSelectColumnNames + ' from 
            (
            select U.FirstName,U.LastName,Sh.[Day],concat(convert(varchar(5),Sh.StartTime,108),''-'', convert(varchar(5),Sh.EndTime,108)) as TimeOfWork from dbo.[User] as U 
            inner join dbo.Employee as E on U.UserId = E.UserId
            inner join dbo.ScheduleStaff as SH on SH.EmployeeId = E.EmployeeId    
            group by U.FirstName, U.LastName,Sh.[Day],StartTime,EndTime 
           ) x
            pivot 
            (
                max(TimeOfWork)
                for [day] in (' + @PivotColumnNames + ')
            ) p'

    execute(@query)
end    
go


