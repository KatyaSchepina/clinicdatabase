use Medcenter
go

drop procedure if exists InfoUserRecordProcedure;
go

create procedure InfoUserRecordProcedure
	@UserFirstName nvarchar(30), @UserLastName nvarchar(30)
as
begin		
	declare @SQL nvarchar(max)
	
	set @SQL =  N'select cast(R.[Day] as date) as [Date], R.Time, Description from dbo.[User] U 
				inner join dbo.Record R
				on U.UserId=R.UserId
				inner join dbo.Specialist S
				on R.SpecialistId=S.SpecialistId
				inner join dbo.Employee E
				on S.EmployeeId=E.EmployeeId
				where U.FirstName = @UserFirstName and U.LastName=@UserLastName';

	exec sp_executesql @SQL,N'@UserFirstName nvarchar(30),@UserLastName nvarchar(30)',@UserFirstName,@UserLastName;
end
go

exec InfoUserRecordProcedure 'Oleg', 'Babichev';
go







