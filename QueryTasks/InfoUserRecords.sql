﻿use Medcenter
go

drop procedure if exists InfoUserRecord;
go

create procedure InfoUserRecord
	@UserFirstName nvarchar(30), @UserLastName nvarchar(30)
as
begin	
	declare @SQL nvarchar(max)
	set @SQL =  N'select U.UserId, FirstName, LastName, [Day] from dbo.[User] U 
				inner join dbo.Record R
				on U.UserId=R.UserId
				where U.FirstName = @UserFirstName';

	EXEC sp_executesql @SQL,N'@UserFirstName nvarchar(30)',@UserFirstName;
end
go

EXEC InfoUserRecord 'Oleg', 'Babichev';


