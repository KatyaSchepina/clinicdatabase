use MedCenter
go

drop table if exists dbo.[Admin]
go

create table dbo.[Admin]
(
    AdminId          int               identity(1,1),
    UserId           int               not null,
    Competence       nvarchar(20)      not null,
    [Description]    nvarchar(1000)    null,

    constraint   Pk_Admin_AdminId primary key(AdminId)
)
go



