use MedCenter
go

drop table if exists dbo.Assistent
go

create table dbo.Assistent
(
    AssistentId     int    identity(1,1),
    EmployeeId      int    not null,
    AdminId         int    not null,
    SpecialistId    int    not null,

    constraint Pk_Assistent_AssistentId primary key(AssistentId)
)
go