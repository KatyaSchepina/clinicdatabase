use MedCenter 
go


drop table if exists dbo.Conclusion
go

create table dbo.Conclusion
(
    ConclusionId      int               identity(1,1),
    MedicalCardId     int               not null,
    ConclusionDate    datetime          not null,
    AssistentId       int               not null,
    SpecialistId      int               not null,
    DiseaseId         int               not null,
    [Description]     nvarchar(1000)    not null,

    constraint Pk_Conclusion_ConclusionId primary key(ConclusionId)
)
go