use MedCenter 
go

drop table if exists dbo.DiseaseList
go

create table dbo.DiseaseList
(
    DiseaseListId    int               identity(1,1),
    Code             int               not null,
    [Description]    nvarchar(1000)    null,

    constraint Pk_DiseaseList_DiseaseListId primary key(DiseaseListId)
)
go