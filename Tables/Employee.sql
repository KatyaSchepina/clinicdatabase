use MedCenter
go

drop table if exists dbo.Employee
go

create table dbo.Employee
(
    EmployeeId       int               identity(1,1),
    UserId           int               not null,
    FilialId         int               not null,
    Position         nvarchar(30)      not null,
    [Description]    nvarchar(1000)    null,
    [Status]         bit               not null,
    DateStart        datetime          not null,

    constraint Pk_Employee_EmployeeId primary key(EmployeeId)
)
go