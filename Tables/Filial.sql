use MedCenter
go

drop table if exists dbo.Filial
go

create table dbo.Filial
(
    FilialId    int              identity(1,1),
    City        nvarchar(20)     not null,
    Street      nvarchar(50)     not null,
    Home        int              not null,

    constraint Pk_Filial_FilialId primary key(FilialId)
)
go