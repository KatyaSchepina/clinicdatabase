use MedCenter
go

drop table if exists dbo.Guest
go

create table dbo.Guest
(
    GuestId             int             identity(1,1),
    IpAdress            nvarchar(12)    not null,
    SessionTime         datetime        not null,
    Browser             nvarchar(15)    not null,
    OperatingSystem     nvarchar(17)    not null,
    Flag                nvarchar(14)    null,
    
    constraint PK_Guest_GuestId primary key(GuestId)
)
go

