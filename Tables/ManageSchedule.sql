use MedCenter 
go

drop table if exists dbo.ManageSchedule
go 

create table dbo.ManageSchedule 
(
    ManageScheduleId    int         identity(1,1),
    ManagerId           int         not null,
    ScheduleStaffId     int         not null,
    CorrectionDate      datetime    not null,

    constraint Pk_ManageSchedule_ManageScheduleId primary key(ManageScheduleId)
)
go