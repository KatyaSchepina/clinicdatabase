use MedCenter
go

drop table if exists dbo.Manager
go

create table dbo.Manager 
(
    ManagerId     int             identity(1,1),
    EmployeeId    int             not null,
    AdminId       int             not null,

    constraint Pk_Manager_MangerId primary key(ManagerId)
)
go