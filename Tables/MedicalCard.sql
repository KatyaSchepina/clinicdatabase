use MedCenter
go

drop table if exists dbo.MedicalCard
go

create table dbo.MedicalCard 
(
    MedicalCardId    int         identity(1,1),
    UserId           int         not null,
    AssistentId      int         not null,
    CreateDate       datetime    not null,

    constraint Pk_MedicalCard_MedicalCardId primary key(MedicalCardId)
)
go