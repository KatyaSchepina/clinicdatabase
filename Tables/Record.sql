use MedCenter
go

drop table if exists dbo.Record
go

create table dbo.Record
(
    RecordId        int         identity(1,1),
    UserId          int         not null,
    SpecialistId    int         not null,
    [Day]           datetime    not null,
    [Time]          time        not null

    constraint Pk_Record_RecordId primary key(RecordId)
)
go
