use MedCenter
go

drop table if exists dbo.Review
go

create table dbo.Review 
(
    ReviewId         int               identity(1,1),
    RecordId         int               not null,
    UserId           int               not null,
    ManagerId        int               not null,
    IsApproved       bit               not null,
    [Description]    nvarchar(1000)    null,

    constraint Pk_Review_ReviewId primary key(ReviewId)
)
go