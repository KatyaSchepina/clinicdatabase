use MedCenter 
go 

drop table if exists dbo.ScheduleStaff
go

create table dbo.ScheduleStaff
(
    ScheduleStaffId    int         identity(1,1),
    EmployeeId         int         not null,
    [Day]              date        not null,
    StartTime          time        not null,
    EndTime            time        not null, 

    constraint Pk_ScheduleStaff_ScheduleStaffId primary key(ScheduleStaffId)
)
go