use MedCenter
go

drop table if exists dbo.Specialist
go

create table dbo.Specialist
(
    SpecialistId      int             identity(1,1),
    EmployeeId        int             not null,
    AdminId           int             not null,

    constraint Pk_Specialist_SpecialistId primary key(SpecialistId)
)
go