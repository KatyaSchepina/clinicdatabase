use MedCenter
go

drop table if exists dbo.[User]
go

create table dbo.[User]
(
    UserId            int             identity(1,1),
    Email             nvarchar(30)    not null,
    PassportNumber    nvarchar(14)    not null,
    FirstName         nvarchar(15)    not null,
    LastName          nvarchar(20)    not null,
    Phone             nvarchar(15)    null,

    constraint PK_dbo_User_UserId primary key(UserId)
)
go
 